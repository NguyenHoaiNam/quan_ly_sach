<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = [
        'id_payment',
        'id_admin',
        'email',
        'so_luong',
        'tien_te',
        'tinh_trang_thanh_toan',
    ];
}
