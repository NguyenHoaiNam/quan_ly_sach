<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TacGia extends Model
{
    protected $table = 'tac_gias';
    protected $fillable = [
        'ma_tac_gia',
        'ten_tac_gia',
        'thong_tin',
        'hinh_anh',
        'trang_thai',
        'tinh_trang',
        'trang_thai'
    ];
}
