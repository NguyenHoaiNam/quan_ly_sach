<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TheLoai extends Model
{
    protected $table = 'the_loais';
    protected $fillable = [
        'ma_the_loai',
        'ten_the_loai',
        'tinh_trang',
    ];
}
