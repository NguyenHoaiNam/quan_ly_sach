<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChiTietHoaDon extends Model
{
    protected $table = 'chi_tiet_hoa_dons';

    protected $fillable = [
        'id_hoa_don_ban_hang',
        'id_admin',
        'id_quan_ly_sach',
        'ten_sach',
        'so_luong_ban',
        'don_gia_ban',
        'tien_chiet_khau',
        'thanh_tien',
    ];
}
