<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoaDonBanHang extends Model
{
    protected $table = 'hoa_don_ban_hangs';

    protected $fillable = [
        'ma_hoa_don_ban_hang',
        'id_admin',
        'ten_sach',//
        'so_luong_mua',//
        'don_gia_mua',//
        'tong_tien',
        'giam_gia',
        'id_quan_ly_sach',
        'trang_thai',
        'id_loai_thanh_toan',
        'ghi_chu_loai_thanh_toan',
        'ngay_thanh_toan',
    ];
}
