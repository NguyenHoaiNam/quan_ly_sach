<?php

namespace App\Http\Requests\KhachHang;

use Illuminate\Foundation\Http\FormRequest;

class CreateKhachHangRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'ho_lot'            => 'nullable',
            'ten_khach'         => 'required',
            'so_dien_thoai'     => 'required|numeric',
            'email'             => 'required|email|unique:admins,email',
            'ghi_chu'           => 'nullable',
            'ngay_sinh'         => 'nullable',
            'id_loai_khach'     => 'required',
            'ma_so_thue'        => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'ten_khach.*'         => 'Tên Khách hông được để trống!',
            'so_dien_thoai.*'     => 'Số điện thoại Không được để trống!',
            'id_loai_khach.*'     => 'Loại kháchkhông được để trống!',
            'email.required'      =>  'Email không được để trống!',
            'email.email'         =>  'Email không đúng định dạng!',
            'email.unique'        =>  'Email đã tồn tại trong hệ thống!',
            'so_dien_thoai.*'     =>  'Số điện thoại phải là 10 số!',
            'ngay_sinh.*'         =>  'Ngày sinh không được để trống!',
        ];
    }
}
