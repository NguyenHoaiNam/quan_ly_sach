<?php

namespace App\Http\Requests\QuanLySach;

use Illuminate\Foundation\Http\FormRequest;

class CheckQuanLySachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'ma_sach'           =>  'required|min:1|max:20',
            'ten_sach'          =>  'required|min:5|max:50',
            'id_the_loai'       =>  'required',
            'gia_ban'           =>  'required',
            'ma_tac_gia'        =>  'required',
            'hinh_anh'          =>  'required',
            'tinh_trang'        =>  'required',
            'trang_thai'        =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'ma_sach.required'                   =>  "Bắt buộc phải có mã sách",
            'ma_sach.min'                        =>  "Mã sách có tối thiểu 5 ký tự",
            'ma_sach.max'                        =>  "Mã sách có tối đa 20 ký tự",
            'ten_sach.*'                         =>  "Bắt buộc phải có Tên Sách",
            'ten_sach.min'                       =>  "Tên Sách có tối thiểu 5 ký tự",
            'ten_sach.max'                       =>  "Tên Sách có tối đa 20 ký tự",
            'id_the_loai.*'                      =>  "Bắt buộc phải có Thể Loại",
            'gia_ban.*'                          =>  "Bắt buộc phải có Giá Bán",
            'ma_tac_gia.*'                       =>  "Bắt buộc phải có Mã Tác Gải",
            'hinh_anh.*'                         =>  "Bắt buộc phải có Hình Ảnh",
            'tinh_trang.*'                       =>  "Bắt buộc phải có Tình Trạng",
            'trang_thai.*'                       =>  "Bắt buộc phải có Trạng Thái",
        ];
    }
}
