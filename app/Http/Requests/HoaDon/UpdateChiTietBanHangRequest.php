<?php

namespace App\Http\Requests\HoaDon;

use Illuminate\Foundation\Http\FormRequest;

class UpdateChiTietBanHangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'                            =>  'required|exists:hoa_don_ban_hangs,id',
            'so_luong_mua'                  =>  'required|numeric|min:1|max:100',
        ];
    }

    public function messages()
    {
        return [
            'id.*'                              =>  'Chi tiết bán hàng không tồn tại!',
            'so_luong_mua.min'                  =>  'Số lượng mua phải lớn hơn 0',
            'so_luong_mua.max'                  =>  'Số lượng mua tối đa là 100',
        ];
    }
}
