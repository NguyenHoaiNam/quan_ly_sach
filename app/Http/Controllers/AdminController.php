<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\ChangePassWordAdminRequest;
use App\Http\Requests\Admin\CreateAdminRequest;
use App\Http\Requests\Admin\DeleteAdminRequest;
use App\Http\Requests\Admin\UpdateAdminRequest;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;

class AdminController extends Controller
{

    public function viewRegister()
    {
        return view('Login.register');
    }

    public function viewLostPass()
    {
        return view('Login.lost_pass');
    }

    public function actionlostPass(Request $request)
    {
        $taiKhoan = Admin::where('email', $request->email)->first();

        $email = $request->input('email');
        if (!empty($email)) {
            if ($taiKhoan) {
                $now    =  Carbon::now();
                $time   =  $now->diffInMinutes($taiKhoan->updated_at);
                if ($taiKhoan->hash_reset || $time > 15) {
                    $taiKhoan->hash_reset = Str::uuid();
                    $taiKhoan->save();
                }
                toastr()->success("Vui lòng kiểm tra Email");
                return redirect('/login');
            } else {
                toastr()->error("Email không khớp với tài khoản");
                return redirect('/lost-pass');
            }
        } else {
            toastr()->error("Bạn chưa nhập email");
            return redirect('/lost-pass');
        }
    }


    public function actionLogOut()
    {

        Auth::guard('admin')->logout();

        toastr()->success("Đã đăng xuất tài khoản");
        return redirect('/login');
    }

    public function viewLogin()
    {
        $check = Auth::guard('admin')->check();
        if ($check) {
            return redirect('/trang-chu');
        } else {
            return view('Login.login');
        }
    }

    public function actionLogin(Request $request)
    {
        $check  = Auth::guard('admin')->attempt([
            'email'    => $request->email,
            'password'    => $request->password,
        ]);

        $email = $request->input('email');
        $password = $request->input('password');
        if (!empty($email)) {
            if (!empty($password)) {
                if ($check) {
                    toastr()->success("Đã đăng nhập thành công");
                    return redirect('/trang-chu');
                } else {
                    toastr()->error("Tên đăng nhập hoặc mật khẩu không đúng");
                    return redirect('/login');
                }
            } else {
                toastr()->error("Bạn chưa nhập password");
                return redirect('/login');
            }
        } else {
            toastr()->error("Bạn chưa nhập email");
            return redirect('/login');
        }

        // // $request->email, $request->password
        // $check =  Auth::guard('admin')->attempt([
        //     'email'     => $request->email,
        //     'password'  => $request->password
        // ]);
        // if ($check) {
        //     toastr()->success("Đã đăng nhập thành công!");
        //     return redirect('/trang-chu');
        // } else {
        //     toastr()->error("Tài khoản hoặc mật khẩu không đúng!");
        //     return redirect('/');
        // }
    }

    ///////////
    public function index()
    {

        return view('rocker.admin.tai_khoan.index',);
    }

    public function store(CreateAdminRequest $request)
    {

        $data = $request->all();
        $data['password'] =  bcrypt($request->password);
        Admin::create($data);

        return response()->json([
            'status'    => true,
            'message'   => 'Đã tạo tài khoản thành công!'
        ]);
    }

    public function getData()
    {
        $list = Admin::get();
        // $list = Admin::leftjoin('quyens', 'admins.id_quyen', 'quyens.id')
        //              ->select('admins.*', 'quyens.ten_quyen')
        //              ->get();
        return response()->json([
            'list'  => $list
        ]);
    }

    public function destroy(DeleteAdminRequest $request)
    {
        // $x          = $this->checkRule(5);
        // if($x)  {
        //     return response()->json([
        //         'status'    => 0,
        //         'message'   => 'Bạn không đủ quyền',
        //     ]);
        // }

        $admin = Admin::where('id', $request->id)->first();
        $admin->delete();
        return response()->json([
            'status'    => true,
            'message'   => 'Đã xóa thành công!',
        ]);
    }

    public function update(UpdateAdminRequest $request)
    {
        // $x          = $this->checkRule(4);
        // if($x)  {
        //     return response()->json([
        //         'status'    => 0,
        //         'message'   => 'Bạn không đủ quyền',
        //     ]);
        // }

        $data    = $request->all();
        $admin = Admin::find($request->id);
        $admin->update($data);

        return response()->json([
            'status'    => true,
            'message'   => 'Đã cập nhật thành công!',
        ]);
    }

    public function changePassword(ChangePassWordAdminRequest $request)
    {
        // $x          = $this->checkRule(3);
        // if($x)  {
        //     return response()->json([
        //         'status'    => 0,
        //         'message'   => 'Bạn không đủ quyền',
        //     ]);
        // }

        $data  =  $request->all();
        if (isset($data['password'])) {
            $admin = Admin::find($request->id);
            $data['password'] = bcrypt($data['password_new']);
            $admin->password  = $data['password'];
            $admin->save();
            return response()->json([
                'status'    => 1,
                'message'   => 'Đã cập nhật mật khẩu thành công!',
            ]);
        } else {
            toastr()->error("Không có mật khẩu được lưu");
        }

        // $data = $request->all();
        // if (isset($data['password'])) {
        //     if ($request->password != $request->password) {
        //         $admin = Admin::find($request->id);
        //         $data['password'] = bcrypt($data['password_new']);
        //         $admin->password  = $data['password'];
        //         $admin->save();
        //     } else if ($request->password == $request->password) {
        //         toastr()->error("Mật khẩu trùng với mật khẩu cũ");
        //     }
        // } else {

        // }
        // return response()->json([
        //     'status'    => 1,
        //     'message'   => 'Đã cập nhật mật khẩu thành công!',
        // ]);
    }

    public function checkEmail(Request $request)
    {
        if (isset($request->id)) {
            $check = Admin::where('email', $request->email)
                ->where('id', '<>', $request->id)
                ->first();
        } else {
            $check = Admin::where('email', $request->email)->first();
        }

        if ($check) {
            return response()->json([
                'status' => false,
                'message' => "email đã tồn tại!",
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => "email có thể sử dụng",
            ]);
        }
    }
}
