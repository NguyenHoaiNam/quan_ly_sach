<?php

namespace App\Http\Controllers;

use App\Models\TacGia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class TacGiaController extends Controller
{
    public function index() {
        return view('rocker.page.tac_gia.index');
    }

    public function getData() {
        $list   =  TacGia::get();
        return response()->json([
            'list' => $list,
        ]);
    }

    public function doiTrangThai(Request $request) {
        $check  =   TacGia::find($request->id);
        if ($check) {
            $check->trang_thai = !$check->trang_thai;
            $check->save();
            return response()->json([
                'status'    => true,
                'message'   => 'Đã đổi trạng thái thành công',
            ]);
        }
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $file = $request->file('hinh_anh');
        $ten_hinh_anh       =   Str::uuid() . '-' . $request->ten_tac_gia . '.' . $file->getClientOriginalExtension();
        $data['hinh_anh']   =   $ten_hinh_anh;
        $file->move('hinh-anh-tac-gia', $ten_hinh_anh);
        TacGia::create($data);

        return response()->json([
            'status'    => true,
            'message'   => "Thêm mới thành công"
        ]);
    }
}
