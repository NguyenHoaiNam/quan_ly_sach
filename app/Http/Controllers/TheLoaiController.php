<?php

namespace App\Http\Controllers;

use App\Models\QuanLySach;
use App\Models\TheLoai;
use Illuminate\Http\Request;
use Psy\Output\Theme;

class TheLoaiController extends Controller
{

    public function index()
    {
        return view('rocker.admin.the_loai.index');
    }


    public function getData()
    {
        $list   =   TheLoai::get();

        return response()->json([
            'list'  => $list,
        ]);
    }

    // Hàm đổi tình trạng
    public function doiTrangThai(Request $request)
    {
        $check  =  TheLoai::find($request->id);

        if ($check) {
            $check->tinh_trang  = !$check->tinh_trang;
            $check->save();
            return response()->json([
                'status'  => true,
                'message'  => "Đã đổi trạng thái thành công",
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'message'  => "Không thể đổi trạng thái",
            ]);
        }
    }

    public function store(Request $request)
    {
        $data   =   $request->all();

        TheLoai::create($data);

        return response()->json([
            'status'  => true,
            'message'  => "Đã thêm mới thể loại thành công",
        ]);
    }

    public function destroy(Request $request)
    {

        $check  =   TheLoai::find($request->id);

        if ($check) {
            $quanLySach  =    QuanLySach::where('id_the_loai', $request->id)->first();

            if ($quanLySach) {
                return response()->json([
                    'status'  => 2,
                    'message'  => "Thể Loại này đang chứa sách, không thể xoá!",
                ]);
            } else {
                $check->delete();

                return response()->json([
                    'status'  => true,
                    'message'  => "Đã xoá thể loại thành công",
                ]);
            }
        } else {
            return response()->json([
                'status'  => false,
                'message'  => "Thể Loại không tồn tại",
            ]);
        }
    }
}
