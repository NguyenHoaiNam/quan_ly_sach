<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuanLySach\CheckQuanLySachRequest;
use App\Models\QuanLySach;
use App\Models\TacGia;
use App\Models\TheLoai;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use function GuzzleHttp\Promise\all;

class QuanLySachController extends Controller
{

    public function index()
    {
        $theLoai = TheLoai::all();
        $tacGia  = TacGia::all();
        return view('rocker.admin.quan_ly_sach.index', compact('theLoai', 'tacGia'));
    }

    // public function getDataTacGia()
    // {
    //     $tacGia = QuanLySach::join('tac_gias', 'quan_ly_sachs.id_tac_gia', 'tac_gias.id')
    //         ->select('quan_ly_sachs.*', 'tac_gias.ten_tac_gia');

    //     return response()->json([
    //         'tacGia' =>  $tacGia->get(),
    //     ]);
    // }
    public function getData(Request $request)
    {
        // $list = QuanLySach::get();
        $list = QuanLySach::join('the_loais', 'quan_ly_sachs.id_the_loai', 'the_loais.id')
                            ->join('tac_gias', 'quan_ly_sachs.ma_tac_gia', 'tac_gias.id')
                            ->select('quan_ly_sachs.*', 'the_loais.ten_the_loai','tac_gias.ten_tac_gia');

        //Thuật toán tìm kiếm theo tên
        $keySearch = $request->input('key_search');
        if (!empty($keySearch)) {
            $list->where('ten_sach', 'like', '%' . $request->key_search . '%')
                ->orWhere('the_loais.ten_the_loai', 'like', '%' . $request->key_search . '%');
        }

        //Thuật toán sắp xếp
        $orderBy = $request->input('order_by');
        if ($orderBy == 1) {
            $list->orderBy('gia_ban', 'asc');
        } elseif ($orderBy == 2) {
            $list->orderBy('gia_ban', 'desc');
        }

        //Thuật toán loadData theo thể loại
        $theloai = $request->input('key_search_the_loai');
        if (!empty($theloai)) {
            $list->where('id_the_loai', $theloai);
        }

        return response()->json([
            'list' =>  $list->get(),
        ]);
    }

    public function doiTrangThai(Request $request)
    {
        $check  =   QuanLySach::find($request->id);
        if ($check) {
            $check->trang_thai = !$check->trang_thai;
            $check->save();
            return response()->json([
                'status'    => true,
                'message'   => 'Đã đổi trạng thái thành công',
            ]);
        }
    }

    public function doiTinhTrang(Request $request)
    {
        $check  =   QuanLySach::find($request->id);
        if ($check) {
            $check->tinh_trang = !$check->tinh_trang;
            $check->save();
            return response()->json([
                'status'    => true,
                'message'   => 'Đã đổi tình trạng thành công',
            ]);
        }
    }

    public function store(CheckQuanLySachRequest $request)
    {
        // dd($request->all());
        $data = $request->all();
        $file = $request->file('hinh_anh');
        $ten_hinh_anh       =   Str::uuid() . '-' . $request->ten_sach . '.' . $file->getClientOriginalExtension();
        $data['hinh_anh']   =   $ten_hinh_anh;
        $file->move('hinh-anh-sach', $ten_hinh_anh);
        QuanLySach::create($data);

        return response()->json([
            'status'    => true,
            'message'   => "Thêm mới thành công"
        ]);
    }

    public function edit(Request $request)
    {
        $quanLySach     =   QuanLySach::find($request->id);

        if ($quanLySach) {
            return response()->json([
                'status'        => true,
                'message'       => "Đã lấy được dữ liệu",
                'quanLySach'    => $quanLySach
            ]);
        } else {
            return response()->json([
                'status'        => false,
                'message'       => "Không lấy được dữ liệu",
            ]);
        }
    }

    public function update(Request $request)
    {
        $quanLySach     =   QuanLySach::where('id', $request->id)->first();

        $data = $request->all();
        $quanLySach->update($data);

        return response()->json([
            'status'    => true,
            'message'   => "Đã cập nhật mới sách thành công"
        ]);
    }

    public function destroy(Request $request)
    {
        $quanLySach    =  QuanLySach::find($request->id);

        if ($quanLySach) {
            $quanLySach->delete();
            return response()->json([
                'status'    => true,
                'message'   => "Đã xoá sách thành công",
            ]);
        } else {
            return response()->json([
                'status'    => true,
                'message'   => "Không thể xoá",
            ]);
        }
    }

    public function checkMaSach(Request $request)
    {
        if (isset($request->id)) {
            $check = QuanLySach::where('ma_sach', $request->ma_sach)
                ->where('id', '<>', $request->id)
                ->first();
        } else {
            $check = QuanLySach::where('ma_sach', $request->ma_sach)->first();
        }

        if ($check) {
            return response()->json([
                'status' => false,
                'message' => "Mã Sách đã tồn tại!",
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => "Mã Sách có thể sử dụng",
            ]);
        }
    }

    public function checkTenSach(Request $request)
    {
        if (isset($request->id)) {
            $check = QuanLySach::where('ten_sach', $request->ten_sach)
                ->where('id', '<>', $request->id)
                ->first();
        } else {
            $check = QuanLySach::where('ten_sach', $request->ten_sach)->first();
        }

        if ($check) {
            return response()->json([
                'status' => false,
                'message' => "Tên Sach đã tồn tại!",
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => "Tên Sach có thể sử dụng",
            ]);
        }
    }
}
