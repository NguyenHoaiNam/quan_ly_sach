<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChiTietHoaDonController;
use App\Http\Controllers\HoaDonBanHangController;
use App\Http\Controllers\KhachHangController;
use App\Http\Controllers\QuanLySachController;
use App\Http\Controllers\TacGiaController;
use App\Http\Controllers\TheLoaiController;
use App\Http\Controllers\TrangChuController;
use App\Models\ChiTietHoaDon;
use App\Models\HoaDonBanHang;
use Illuminate\Support\Facades\Route;

// Route::middleware(['auth:sanctum'])->get('/trang-chu', function () {
//         return view('rocker.page.dao_dien.index');
// });
Route::get('/trang-chu', [TrangChuController::class, 'index']);

Route::get('/login', [AdminController::class, 'viewLogin']);
Route::post('/admin/login', [AdminController::class, 'actionLogin']);
Route::get('/logout', [AdminController::class, 'actionLogOut']);


Route::get('/lost-pass', [AdminController::class, 'viewLostPass']);
Route::post('/admin/lost-pass', [AdminController::class, 'actionlostPass']);

Route::get('/register', [AdminController::class, 'viewRegister']);


Route::group(['prefix' => '/admin', 'middleware' => 'checkLogin'], function () {
    Route::get('/logout', [AdminController::class, 'actionLogOut']);
    //Quản Lý Sach
    Route::get('/quan-ly-sach/data', [QuanLySachController::class, 'getData']);
    Route::group(['prefix' => '/quan-ly-sach', 'middleware' => 'checkLogin'], function () {
        Route::get('/', [QuanLySachController::class, 'index']);
        Route::post('/doi-trang-thai', [QuanLySachController::class, 'doiTrangThai']);
        Route::post('/doi-tinh-trang', [QuanLySachController::class, 'doiTinhTrang']);
        Route::post('/create', [QuanLySachController::class, 'store']);
        Route::post('/edit', [QuanLySachController::class, 'edit']);
        Route::post('/update', [QuanLySachController::class, 'update']);
        Route::post('/delete', [QuanLySachController::class, 'destroy']);
        Route::post('/check-ma-sach', [QuanLySachController::class, 'checkMaSach']);
        Route::post('/check-ten-sach', [QuanLySachController::class, 'checkTenSach']);
    });

    Route::get('/the-loai/data', [TheLoaiController::class, 'getData']);
    Route::group(['prefix' => '/the-loai', 'middleware' => 'checkLogin'], function () {
        Route::get('/', [TheLoaiController::class, 'index']);
        Route::post('/doi-trang-thai', [TheLoaiController::class, 'doiTrangThai']);
        Route::post('/create', [TheLoaiController::class, 'store']);
        Route::post('/edit', [TheLoaiController::class, 'edit']);
        Route::post('/update', [TheLoaiController::class, 'update']);
        Route::post('/delete', [TheLoaiController::class, 'destroy']);
    });

    Route::get('/tac-gia/data', [TacGiaController::class, 'getData']);
    Route::group(['prefix' => '/tac-gia', 'middleware' => 'checkLogin'], function () {
        Route::get('/', [TacGiaController::class, 'index']);
        Route::post('/doi-trang-thai', [TacGiaController::class, 'doiTrangThai']);
        Route::post('/create', [TacGiaController::class, 'store']);
        Route::post('/edit', [TacGiaController::class, 'edit']);
        Route::post('/update', [TacGiaController::class, 'update']);
        Route::post('/delete', [TacGiaController::class, 'destroy']);
    });

    Route::get('/khach-hang/data', [KhachHangController::class, 'getData']);
    Route::group(['prefix' => '/khach-hang', 'middleware' => 'checkLogin'], function () {
        Route::get('/', [KhachHangController::class, 'index']);
        Route::post('/create', [KhachHangController::class, 'store']);
        Route::post('/update', [KhachHangController::class, 'update']);
        Route::post('/delete', [KhachHangController::class, 'delete']);
    });

    Route::group(['prefix' => '/ban-hang'], function () {
        Route::get('/', [HoaDonBanHangController::class, 'index']);
        Route::get('/the-loai-sach/{id}', [HoaDonBanHangController::class, 'indexTheLoai']);
        Route::get('/tac-gia/{id}', [HoaDonBanHangController::class, 'indexTacGia']);
        Route::post('/update', [HoaDonBanHangController::class, 'update']);
    });

    Route::group(['prefix' => '/chi-tiet-san-pham'], function () {
        Route::get('/', [HoaDonBanHangController::class, 'indexChiTiet']);
        Route::get('/{id}', [HoaDonBanHangController::class, 'indexGetChiTietSanPham']);
        Route::post('/gio-hang', [HoaDonBanHangController::class, 'ByGioHang']);
    });

    Route::group(['prefix' => '/gio-hang'], function () {
        Route::get('/', [ChiTietHoaDonController::class, 'indexGioHang']);
        Route::get('/data-gio-hang', [HoaDonBanHangController::class, 'getDataGioHang']);
        Route::post('/delete', [HoaDonBanHangController::class, 'destroy']);

    });

    Route::get('/tai-khoan/data', [AdminController::class, 'getData']);
    Route::group(['prefix' => '/tai-khoan',], function () {
        Route::get('/', [AdminController::class, 'index']);
        Route::post('/create', [AdminController::class, 'store']);
        Route::post('/delete', [AdminController::class, 'destroy']);
        Route::post('/update', [AdminController::class, 'update']);
        Route::post('/change-password', [AdminController::class, 'changePassword']);
        Route::post('/check-email', [AdminController::class, 'checkEmail']);

    });
});
