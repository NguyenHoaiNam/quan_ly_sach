<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quan_ly_sachs', function (Blueprint $table) {
            $table->id();
            $table->string('ma_sach');
            $table->string('ten_sach');
            $table->string('id_the_loai');
            $table->integer('gia_ban');
            $table->integer('ma_tac_gia');
            $table->string('hinh_anh')->nullable();
            $table->text('thong_tin');
            $table->integer('tinh_trang');
            $table->integer('trang_thai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quan_ly_saches');
    }
};
