<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tac_gias', function (Blueprint $table) {
            $table->id();
            $table->string('ma_tac_gia');
            $table->string('ten_tac_gia');
            $table->text('thong_tin');
            $table->string('hinh_anh')->nullable();
            $table->integer('tinh_trang')->default(0);
            $table->integer('trang_thai')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tac_gias');
    }
};
