<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="/assets_admin/images/favicon-32x32.png" type="image/png" />
    <!--plugins-->
    <link href="/assets_admin/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="/assets_admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="/assets_admin/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <!-- loader-->
    <link href="/assets_admin/css/pace.min.css" rel="stylesheet" />
    <script src="/assets_admin/js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="/assets_admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets_admin/css/bootstrap-extended.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="/assets_admin/css/app.css" rel="stylesheet">
    <link href="/assets_admin/css/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" />
    {{--  --}}
    <!--end switcher-->
    <!-- Bootstrap JS -->
    <script src="/assets_admin/js/bootstrap.bundle.min.js"></script>
    <!--plugins-->
    <script src="/assets_admin/js/jquery.min.js"></script>
    <script src="/assets_admin/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="/assets_admin/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="/assets_admin/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="/assets_admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="/assets_admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/assets_admin/plugins/chartjs/js/Chart.min.js"></script>
    <script src="/assets_admin/plugins/chartjs/js/Chart.extension.js"></script>
    <script src="/assets_admin/js/index.js"></script>
    <!--app JS-->
    <script src="/assets_admin/js/app.js"></script>
    {{-- Đoạn JS Thêm Riêng --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.4/axios.min.js"
        integrity="sha512-LUKzDoJKOLqnxGWWIBM4lzRBlxcva2ZTztO8bTcWPmDSpkErWx0bSP4pdsjNH8kiHAUPaT06UXcb+vOEZH+HpQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
        integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <title>Đăng Ký</title>
</head>

<body class="bg-register">
    <!--wrapper-->
    <div class="wrapper">
        <div class="row" id="app">
            <div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-0">
                <div class="container-fluid">
                    <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                        <div class="col mx-auto">
                            <div class="card">
                                <div class="card-body">
                                    <div class="border p-4 rounded">
                                        <div class="text-center">
                                            <h3 class="">Đăng Ký</h3>
                                        </div>
                                            <div class="mb-3">
                                                <label class="form-label">Họ Và Tên</label>
                                                <input v-model="add.ho_va_ten" type="text" class="form-control"
                                                    placeholder="Nhập vào họ và tên *">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Email</label>
                                                <input v-model="add.email" type="email" class="form-control"
                                                    placeholder="Nhập vào email *">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Số Điện Thoại</label>
                                                <input v-model="add.so_dien_thoai" type="tel" class="form-control"
                                                    placeholder="Nhập vào số điện thoại *">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Ngày Sinh</label>
                                                <input v-model="add.ngay_sinh" type="date" class="form-control">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Mật Khẩu</label>
                                                <input v-model="add.password" type="password" class="form-control"
                                                    placeholder="Nhập vào mật khẩu *">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Xác Nhận Mật Khẩu</label>
                                                <input v-model="add.re_password" type="password" class="form-control"
                                                    placeholder="Nhập lại mật khẩu *">
                                            </div>
                                            <div class="mb-3 text-end"> <a href="/login">Đăng Nhập</a>
                                            </div>
                                            {{-- <div class="mb-3">
                                            <label class="form-label">Quyền Tài Khoản</label>
                                            <select v-model="add.id_quyen" class="form-control">
                                                <option value="0">admin</option>
                                                @foreach ($quyen as $key => $value)
                                                    <option value="{{$value->id}}">{{$value->ten_quyen}}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                        <div class=" text-center">
                                            <button class="btn btn-primary" type="button"
                                                v-on:click="CreateTaiKhoan()">Đăng Ký</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
    </div>

    {{-- script --}}
    <script>
        new Vue({
            el: "#app",
            data: {
                add: {},
            },
            created() {
                this.loadData();
            },
            methods: {
                date_format(now) {
                    return moment(now).format('DD/MM/yyyy');
                },
                CreateTaiKhoan() {
                    axios
                        .post('/admin/tai-khoan/create', this.add)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadData();
                            }
                        })
                        .catch((res) => {
                            $.each(res.response.data.errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },
            }
        });
    </script>
</body>

</html>


{{-- @extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-4"></div>
        <div class="col-md-4">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header text-center">
                    <b>Đăng Ký</b>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Họ Và Tên</label>
                        <input v-model="add.ho_va_ten" type="text" class="form-control" placeholder="Nhập vào họ và tên *">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input v-model="add.email" type="email" class="form-control" placeholder="Nhập vào email *">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Số Điện Thoại</label>
                        <input v-model="add.so_dien_thoai" type="tel" class="form-control" placeholder="Nhập vào số điện thoại *">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Ngày Sinh</label>
                        <input v-model="add.ngay_sinh" type="date" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Mật Khẩu</label>
                        <input v-model="add.password" type="password" class="form-control" placeholder="Nhập vào mật khẩu *">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Xác Nhận Mật Khẩu</label>
                        <input v-model="add.re_password" type="password" class="form-control" placeholder="Nhập lại mật khẩu *">
                    </div>
                    {{-- <div class="mb-3">
                        <label class="form-label">Quyền Tài Khoản</label>
                        <select v-model="add.id_quyen" class="form-control">
                            <option value="0">admin</option>
                            @foreach ($quyen as $key => $value)
                                <option value="{{$value->id}}">{{$value->ten_quyen}}</option>
                            @endforeach
                        </select>
                    </div> --}}
{{-- </div>
                <div class="card-footer text-center">
                    <button class="btn btn-primary" type="button" v-on:click="CreateTaiKhoan()">Đăng Ký</button>
                </div>
            </div>
        </div>
        <div class="col-4"></div>
    </div>
@endsection --}}
