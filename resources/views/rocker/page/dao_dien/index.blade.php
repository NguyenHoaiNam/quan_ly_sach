@extends('share.master')
@section('noi_dung')
    <body class=" pace-done">
        <div id="app">
            <div class="alert  " style="background: rgb(255, 255, 255);" role="alert">
                <marquee class="" behavior="" direction="">
                   <b>Rất vinh hạnh được đón chào</b>
                </marquee>
            </div>
            <!--Slide-->
            <div class="row">
                <div id="carouselExampleControlsNoTouching" class="carousel slide" data-bs-touch="false">
                    <div class="carousel-inner">
                        <div class="video text-center">
                            <iframe width="1200" height="505" src="https://www.youtube.com/embed/imNbIwVJ1lA"
                                title="Tại Sao Cần Phải Đọc Sách? Những Lợi Ích Của Việc Đọc Sách Mà Bạn Cần Biết | Nghĩ Lớn"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowfullscreen></iframe>
                        </div>
                    </div>

                </div>
                <!-- -->
                <div class="alert mt-2" style="background: rgb(255, 255, 255);" role="alert">
                    <marquee class="" behavior="" direction="">
                        <b>Rất vinh hạnh được đón chào</b>
                    </marquee>
                </div>
                {{-- end Slide --}}
                <h6 class="mb-0 text-uppercase">Một Số Sách Bán Chạy Nhất</h6>
                <hr>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4">
                    <template v-for="(value,key) in list_sach" v-if="value.trang_thai == 1">
                        <div class="col">
                            <div class="card border-primary border-bottom border-3 border-0" style="height: 480px">
                                <div class="card-header">
                                    <i class="text-center"><img class="img-fluid"
                                            v-bind:src="'/hinh-anh-sach/' + value.hinh_anh" alt="aaaa"></i>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title text-primary text-center">@{{ value.ten_sach }} </h5>
                                    {{-- <hr> --}}
                                    {{-- <div class="text-center">
                                    <a href="" class="btn btn-inverse-primary"><i
                                            class="bx bx-star"></i>Button</a>
                                    <a href="javascript:" class="btn btn-primary"><i
                                            class="bx bx-microphone"></i>Button</a>
                                </div> --}}
                                </div>
                            </div>
                        </div>
                    </template>
                </div>
                <!--end row-->
                <h6 class="mb-0 text-uppercase">Tác Giả Nổi Bật</h6>
                <hr>
                <div class="row row-cols-1 row-cols-md-1 row-cols-lg-3 row-cols-xl-3">
                    <template v-for="(value, key) in list_tac_gia">
                        <div class="col">
                            <div class="card" style="height: 550px;">
                                <div class="card-header">
                                    <i class="text-center"><img style="height: 300px;"
                                            v-bind:src="'/hinh-anh-tac-gia/' + value.hinh_anh"
                                            class="card-img-top img-fluid" alt="..."></i>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title"> @{{ value.ten_tac_gia }} </h5>
                                    <p class="card-text"> @{{ value.thong_tin }} </p>
                                </div>
                                {{-- <div class="card-body"> <a href="#" class="card-link">Card link</a>
                                <a href="#" class="card-link">Another link</a>
                            </div> --}}
                            </div>
                        </div>
                    </template>
                </div>
                <!--end row-->
                <h6 class="mb-0 text-uppercase">Card with background color</h6>
                <hr>
                <div class="row row-cols-1 row-cols-md-1 row-cols-lg-3 row-cols-xl-3">
                    <div class="col d-flex">
                        <div class="card bg-danger w-100">
                            <img src="assets_admin/images/gallery/35.png" class="card-img-top" alt="...">
                            <div class="card-body text-white">
                                <h5 class="card-title text-white">Card Sample Title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural
                                    lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small>Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex">
                        <div class="card bg-success w-100">
                            <img src="assets_admin/images/gallery/36.png" class="card-img-top" alt="...">
                            <div class="card-body text-white">
                                <h5 class="card-title text-white">Card Sample Title</h5>
                                <p class="card-text">This card has supporting text below as a natural lead-in to
                                    additional content.</p>
                                <p class="card-text"><small>Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex">
                        <div class="card bg-warning w-100">
                            <img src="assets_admin/images/gallery/37.png" class="card-img-top" alt="...">
                            <div class="card-body text-white">
                                <h5 class="card-title text-white">Card Sample Title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural
                                    lead-in to additional content. This card has even longer content than the first to
                                    show that equal height action.</p>
                                <p class="card-text"><small>Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>

                <h6 class="mb-0 text-uppercase">Card Group</h6>
                <hr>
                <div class="card-group shadow">
                    <div class="card border-end shadow-none">
                        <img src="assets_admin/images/gallery/14.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in
                                to additional content. This content is a little bit longer.</p>
                            <hr>
                            <div class="d-flex align-items-center gap-2">
                                <a href="javascript:;" class="btn btn-inverse-dark"><i class="bx bx-star"></i>Button</a>
                                <a href="javascript:;" class="btn btn-dark"><i class="bx bx-microphone"></i>Button</a>
                            </div>
                        </div>
                        <div class="card-footer bg-white"> <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                    </div>
                    <div class="card border-end shadow-none">
                        <img src="assets_admin/images/gallery/15.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in
                                to additional content. This content is a little bit longer.</p>
                            <hr>
                            <div class="d-flex align-items-center gap-2">
                                <a href="javascript:;" class="btn btn-inverse-warning"><i
                                        class="bx bx-star"></i>Button</a>
                                <a href="javascript:;" class="btn btn-warning"><i class="bx bx-microphone"></i>Button</a>
                            </div>
                        </div>
                        <div class="card-footer bg-white"> <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                    </div>
                    <div class="card shadow-none">
                        <img src="assets_admin/images/gallery/16.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in
                                to additional content. This content is a little bit longer.</p>
                            <hr>
                            <div class="d-flex align-items-center gap-2">
                                <a href="javascript:;" class="btn btn-inverse-info"><i class="bx bx-star"></i>Button</a>
                                <a href="javascript:;" class="btn btn-info"><i class="bx bx-microphone"></i>Button</a>
                            </div>
                        </div>
                        <div class="card-footer bg-white"> <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                    </div>
                </div>
            </div>
    </body>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list_sach: [],
                    list_tac_gia: [],
                },
                created() {
                    this.loadDataSach();
                    this.loadDataTacGia();
                },
                methods: {
                    // Hàm đẩy dữ liệu lên serve
                    loadDataSach() {
                        axios
                            .get('/admin/quan-ly-sach/data')
                            .then((res) => {
                                this.list_sach = res.data.list;
                            });
                    },
                    loadDataTacGia() {
                        axios
                            .get('/admin/tac-gia/data')
                            .then((res) => {
                                this.list_tac_gia = res.data.list;
                            });
                    },
                }
            });
        });
    </script>
@endsection
