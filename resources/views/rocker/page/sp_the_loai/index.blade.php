@extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg col-xl-10">
                                <div class="row row-cols-lg-2 row-cols-xl-auto g-2">
                                    <div class="col">
                                        <div class="position-relative">
                                            <input v-model="key_search" v-on:keyup.enter="search()" v-on:blur="search()"
                                                type="text" class="form-control ps-5" placeholder="Tìm kiếm...">
                                            <span class="position-absolute top-50 product-show translate-middle-y"><i
                                                    class="bx bx-search"></i></span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div v-on:click="sort()" class="btn-group" role="group"
                                            aria-label="Button group with nested dropdown">
                                            <button type="button" class="btn btn-outline-dark">Sắp Xếp Theo</button>
                                            <div class="btn-group" role="group">
                                                <button v-if="order_by == 1" id="btnGroupDrop1" type="button"
                                                    class="btn btn-outline-dark dropdown-toggle dropdown-toggle-nocaret px-1"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa-solid fa-arrow-up"></i>
                                                </button>
                                                <button v-else-if="order_by == 2" id="btnGroupDrop1" type="button"
                                                    class="btn btn-outline-dark dropdown-toggle dropdown-toggle-nocaret px-1"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa-solid fa-solid fa-arrow-down"></i>
                                                </button>
                                                <button v-else id="btnGroupDrop1" type="button"
                                                    class="btn btn-outline-dark dropdown-toggle dropdown-toggle-nocaret px-1"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa-solid fa-solid fa-spinner fa-pulse"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 row-cols-xl-4 row-cols-xxl-5 product-grid">
            @foreach ($spTheoLoai as $value)
                <div class="col-md">
                    <div class="card" style="height: 550px">
                        <i><img class="img-fluid" src="/hinh-anh-sach/{{ $value->hinh_anh }}" alt="aaaa"></i>
                        <div class="">
                            <div class="position-absolute top-0 end-0 m-3 product-discount"><span
                                    class=""><b>{{ $value->ten_the_loai }}</b></span>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title cursor-pointer"> {{ $value->ten_sach }} </h6>
                            <div class="clearfix">
                                <p class="mb-0 float-end fw-bold"><span
                                        class="me-2 text-decoration-line-through text-secondary">
                                        {{ number_format($value->gia_ban) }} </span><span> {{ number_format($value->gia_ban) }} </span>
                                </p>
                            </div>
                            <div class="d-flex align-items-center mt-3 fs-6">
                                <div class="cursor-pointer text-nowrap">
                                    <i class="bx bxs-star text-warning"></i>
                                    <i class="bx bxs-star text-warning"></i>
                                    <i class="bx bxs-star text-warning"></i>
                                    <i class="bx bxs-star text-warning"></i>
                                    <i class="bx bxs-star text-secondary"></i>
                                </div>
                                <p class="mb-0 ms-auto"></p>
                            </div>
                        </div>
                        <div class="card-footer text-center align-middle text-nowrap">
                            <a  href="" target="_blank" class="btn btn-sm btn-outline-primary "><i></i>Mua Ngay</a>
                            <a href="" class="btn btn-outline-primary "><i
                                    class="fa-solid fa-cart-shopping fa-lg"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list_sach: [],
                    key_search: '',
                    order_by: 0,
                },
                created() {
                    this.loadData();
                },
                methods: {

                    // Hàm đẩy dữ liệu lên serve
                    loadData() {
                        axios
                            .get('/admin/quan-ly-sach/data', {
                                params: {
                                    key_search: this.key_search,
                                    order_by: this.order_by,
                                    key_search_the_loai: this.key_search_the_loai,
                                }
                            })
                            .then((res) => {
                                this.list_sach = res.data.list;
                            });
                    },

                    //Hàm tìm kiếm
                    search() {
                        this.loadData();
                    },
                    //Hàm sắp xếp
                    sort() {
                        this.order_by = this.order_by + 1;
                        if (this.order_by > 2) {
                            this.order_by = 0;
                        }
                        this.loadData();
                    },
                }
            });
        });
    </script>
@endsection
