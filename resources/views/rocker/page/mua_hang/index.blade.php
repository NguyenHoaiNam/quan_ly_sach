@extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="card">
            {{-- <template v-for>

                </template> --}}
            <div class="row g-0">
                <div class="col-md-3 border-end">
                    <img src="/hinh-anh-sach/{{ $sanPham->hinh_anh }}" class="img-fluid" alt="..."
                        style="height: 470px;width: 400px;">
                </div>
                <div class="col-md-9">
                    <div class="card-body">
                        <h4 class="card-title"> {{ $sanPham->ten_sach }}</h4>
                        <div class="d-flex gap-3 py-3">
                            <div class="cursor-pointer">
                                <i class="bx bxs-star text-warning"></i>
                                <i class="bx bxs-star text-warning"></i>
                                <i class="bx bxs-star text-warning"></i>
                                <i class="bx bxs-star text-warning"></i>
                                <i class="bx bxs-star text-secondary"></i>
                            </div>
                            <div>142 reviews</div>
                            <div class="text-success"><i class="bx bxs-cart-alt align-middle"></i> 134 orders</div>
                        </div>
                        <div class="mb-3">
                            <span class="price h4">{{ number_format($sanPham->gia_ban) }}</span>
                            <span class="text-muted">vnđ</span>
                        </div>
                        <h3 class="card-text fs-6"> <b>Thông tin:</b> </h3>
                        <p class="card-text fs-6"> {{ $sanPham->thong_tin }} </p>
                        <dl class="row">
                            <dt class="col-sm-3">Tác Giả: {{ $tacGia->ten_tac_gia }}</dt>
                            <dd class="col-sm-9"> </dd>

                            <dt class="col-sm-3">Thể Loại: {{ $theLoai->ten_the_loai }} </dt>
                        </dl>
                        <hr>
                        <div class="row row-cols-auto row-cols-1 row-cols-md-3 align-items-center">
                            <div class="col">
                                <label class="form-label">Số Lượng</label>
                                <div class="input-group input-spinner">
                                    <button class="btn btn-white" type="button" id="button-minus"
                                        @click="so_luong -= 1">−</button>
                                    <input type="number" class="form-control" v-model="so_luong">
                                    <button class="btn btn-white" type="button" id="button-plus"
                                        @click="so_luong += 1">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex gap-3 mt-3">
                            <template v-if="so_luong > 0">
                                <a v-on:click="byGioHang()" class="btn btn-primary">Xác Nhận</a>
                                <a v-on:click="byGioHang()" target="_blank" href="/admin/gio-hang"
                                    class="btn btn-outline-primary"><span class="text"></span> <i
                                        class="bx bxs-cart-alt"></i></a>
                            </template>
                            <template v-if="so_luong <=0">
                                <a v-on:click="byGioHang()" class="btn btn-primary" disabled>Xác Nhận</a>
                                <a v-on:click="byGioHang()" target="_blank" href="/admin/gio-hang"
                                    class="btn btn-outline-primary" disabled><span class="text"></span> <i
                                        class="bx bxs-cart-alt"></i></a>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-body">
                <ul class="nav nav-tabs nav-primary mb-0" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-bs-toggle="tab" href="#primaryhome" role="tab"
                            aria-selected="true">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class="bx bx-comment-detail font-18 me-1"></i>
                                </div>
                                <div data-bs-toggle="modal"data-bs-target="#deleteModal" class="tab-title"> Đọc Thử </div>
                            </div>
                        </a>
                    </li>
                    {{-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-bs-toggle="tab" href="#primaryprofile" role="tab" aria-selected="false"
                            tabindex="-1">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class="bx bx-bookmark-alt font-18 me-1"></i>
                                </div>
                                <div class="tab-title">Tags</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-bs-toggle="tab" href="#primarycontact" role="tab" aria-selected="false"
                            tabindex="-1">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class="bx bx-star font-18 me-1"></i>
                                </div>
                                <div class="tab-title">Reviews</div>
                            </div>
                        </a>
                    </li> --}}
                </ul>
                <div class="tab-content pt-3">
                    <div class="tab-pane fade show active" id="primaryhome" role="tabpanel">

                    </div>
                    <div class="tab-pane fade" id="primaryprofile" role="tabpanel">

                    </div>
                    <div class="tab-pane fade" id="primarycontact" role="tabpanel">

                    </div>
                </div>
            </div>

        </div>
        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="max-width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center">
                        <h3>{{ $sanPham->ten_sach }}</h3>
                        <b>Tác Giả: </b> {{ $tacGia->ten_tac_gia }}
                        <div>
                            {{ $sanPham->doc_thu }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" v-on:click="accpectDelete()"
                            data-bs-dismiss="modal">Xác Nhận</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: '#app',
            data: {
                value: 1,
                so_luong: 1,
                don_gia_mua: {{ $sanPham->gia_ban }},
                ten_sach: '{{ $sanPham->ten_sach }}',
                id: {{ $sanPham->id }},
            },
            created() {},
            methods: {
                byGioHang() {
                    var payLoad = {
                        'id': this.id,
                        'ten_sach': this.ten_sach,
                        'so_luong': this.so_luong,
                        'don_gia_mua': this.don_gia_mua
                    };
                    axios
                        .post('/admin/chi-tiet-san-pham/gio-hang', payLoad)
                        .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message);
                                } else if (res.data.status == 2) {
                                    toastr.warning(res.data.message, "Warning");
                            } else if (res.data.status == 0) {
                                toastr.errors(res.data.message);
                            }
                        })
                .catch((res) => {
                    $.each(res.response.data.errors, function(k, v) {
                        toastr.error(v[0]);
                    });
                });
            },
        },
        });
    </script>
@endsection
