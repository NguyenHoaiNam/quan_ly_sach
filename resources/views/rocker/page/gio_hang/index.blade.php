@extends('share.master')
@section('noi_dung')
    <div class="card" id="app">
        <div class="card-body">
            <div class="d-lg-flex align-items-center mb-4 gap-3">
                <div class="position-relative">
                    <input type="text" class="form-control ps-5 radius-30" placeholder="Search Order"> <span
                        class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                </div>
                <div class="ms-auto"><a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i
                            class="bx bxs-plus-square"></i>Thanh Toán</a></div>
            </div>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="table-light">
                        <tr>
                            <th class="text-center align-middle">#</th>
                            <th class="text-center align-middle">Tên Sách</th>
                            <th class="text-center align-middle" style="width: 20%;" >Số Lượng</th>
                            <th class="text-center align-middle">Đơn Giá</th>
                            <th class="text-center align-middle">Giảm Giá</th>
                            <th class="text-center align-middle">Thành TIền</th>
                            <th class="text-center align-middle">ghi Chú</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-for="(value, key) in list">
                            <tr>
                                <td class="text-center align-middle">
                                    <input class="form-check-input me-3" type="checkbox" value="" aria-label="...">
                                </td>
                                <td class="text-center align-middle"> @{{ value.ten_sach }} </td>
                                <td class="text-center align-middle" >
                                    <input v-on:change="update(value)" v-model="value.so_luong_mua" type="number" class="form-control text-center" >
                                </td>
                                {{-- <td class="text-center align-middle"> @{{ value.so_luong_mua }} </td> --}}
                                <td class="text-center align-middle"> @{{ number_format(value.don_gia_mua) }} </td>
                                <td class="text-center align-middle" >
                                    <input v-on:change="update(value)" v-model="value.giam_gia" type="number" class="form-control text-center" >
                                </td>
                                <td class="text-center align-middle"> @{{ number_format(value.tong_tien) }}</td>
                                <td class="text-center align-middle" >
                                    <input v-on:change="update(value)" v-model="value.ghi_chu_loai_thanh_toan" type="text" class="form-control text-center" >
                                </td>
                                <td>
                                    <a class="text-center align-middle" href="javascript:;" class=""><i
                                            class="bx bxs-edit"></i></a>
                                    <a v-on:click="del_sach = value" data-bs-toggle="modal"data-bs-target="#deleteModal" href="javascript:;" class="ms-3"><i
                                            class="bx bxs-trash"></i></a>
                                </td>
                            </tr>
                        </template>
                    </tbody>
                </table>
                <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Xóa Khu Vục</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-primary" role="alert">
                                    Bạn có chắc chắn muốn xóa sách: <b
                                        class="text-danger text-uppercase">@{{ del_sach.ten_sach }}</b> này không?
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" v-on:click="accpectDelete()"
                                    data-bs-dismiss="modal">Xác Nhận</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list: [],
                    del_sach: '',
                },
                created() {
                        this.loadData();
                },
                methods: {
                    number_format(number) {
                        return new Intl.NumberFormat('vi-VI', {
                            style: 'currency',
                            currency: 'VND'
                        }).format(number);
                    },
                    loadData() {
                        axios
                            .get('/admin/gio-hang/data-gio-hang')
                            .then((res) => {
                                this.list = res.data.data;
                            });
                    },
                    date_format(now) {
                        return moment(now).format('HH:mm:ss');
                    },
                    accpectDelete() {
                        axios
                            .post('/admin/gio-hang/delete', this.del_sach)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    $('#updateModal').modal('hide');
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    update(v) {
                    axios
                        .post('/admin/ban-hang/update', v)
                        .then((res) => {
                            if(res.data.status == 1) {
                                toastr.success(res.data.message);
                            } else if (res.data.status == 2) {
                                    toastr.warning(res.data.message, "Warning");
                            } else if (res.data.status == 0) {
                                toastr.errors(res.data.message);
                            }
                            this.loadData();
                        })
                        .catch((res) => {
                            $.each(res.response.data.errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },
                },
            });
        });
    </script>
@endsection
