@extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-md-" v-if="trang_thai == 0">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header text-center">
                    <b>Thêm Mới Sách</b>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Mã Sách</label>
                        <input v-model="add_sach.ma_sach" v-on:blur="checkMaSach()" type="text" class="form-control"
                            placeholder="Nhập mã sách">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tên Sách</label>
                        <input v-model="add_sach.ten_sach" v-on:blur="checkTenSach()" type="text" class="form-control"
                            placeholder="Nhập tên sách">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Thể Loại</label>
                        <select v-model="add_sach.id_the_loai" class="form-control">
                            <option value="0">Vui lòng chọn thể loại sách</option>
                            @foreach ($theLoai as $key => $value)
                                <option value="{{ $value->id }}">{{ $value->ten_the_loai }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Giá Bán</label>
                        <input v-model="add_sach.gia_ban" type="text" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Mã Tác Giả</label>
                        <select v-model="add_sach.ma_tac_gia" class="form-control">
                            <option value="0">Vui lòng chọn tác giả</option>
                            @foreach ($tacGia as $value)
                            <option value="{{$value->id}}">{{$value->ten_tac_gia}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-goup mb-3 ">
                        <label class="form-label">Ảnh</label>
                        <input class="form-control " type="file" ref="files" v-on:change="updateFile()"
                            accept="image/*">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Thông Tin</label>
                        <textarea v-model="add_sach.thong_tin" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tình Trạng</label>
                        <select v-model="add_sach.tinh_trang" class="form-control">
                            <option value="-1">Vui lòng chọn tình trạng</option>
                            <option value="1">Đang Bán</option>
                            <option value="0">Dừng Bán</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Trạng Thái</label>
                        <select v-model="add_sach.trang_thai" class="form-control">
                            <option value="-1">Vui lòng chọn trạng thái</option>
                            <option value="1">Trang Chủ</option>
                            <option value="0">Bán hàng</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button id="add" v-on:click="addSach()" class="btn btn-primary ">Thêm Mới</button>
                    <button v-on:click="trang_thai = 1" class="btn btn-info text-end">Thông Tin Sách</button>
                </div>
            </div>
        </div>
        <div class="col-md-" v-if="trang_thai == 1">
            <div class="card">
                <div class="card-header text-center">
                    <b> Danh Sách Các Loại Sách</b>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="listNhaCungCap">
                        <thead>
                            <button v-on:click="trang_thai = 0" class="btn btn-primary mb-2">Thêm Mới Sách</button>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Mã Sách</th>
                                <th class="text-center">Tên Sách</th>
                                <th class="text-center">Thể Loại</th>
                                <th class="text-center">Giá Bán</th>
                                <th class="text-center">Tác Giả</th>
                                <th class="text-center">Thông tin</th>
                                <th class="text-center">Hình Ảnh</th>
                                <th class="text-center">Tình Trạng</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(value, key) in list">
                                <tr>
                                    <th class="text-center align-middle">
                                        <input type="checkbox" v-model="value.check">
                                    </th>
                                    <td class="align-middle text-nowrap"> @{{ value.ma_sach }} </td>
                                    <td class="align-middle "> @{{ value.ten_sach }} </td>
                                    <td class="align-middle "> @{{ value.ten_the_loai }} </td>
                                    <td class="align-middle "> @{{ number_format(value.gia_ban) }} </td>
                                    <td class="align-middle text-nowrap"> @{{ value.ten_tac_gia }} </td>
                                    <td class="align-middle "> @{{ value.thong_tin }} </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <img v-bind:src="'/hinh-anh-sach/' + value.hinh_anh"
                                            style="width: 70px; height: 70px;" alt="">
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <button v-on:click="doiTinhTrang(value)" type="button"
                                            class=" btn btn-outline-info" v-if="value.tinh_trang == 1">Đang Bán</button>
                                        <button v-on:click="doiTinhTrang(value)" type="button"
                                            class=" btn btn-outline-danger" v-else>Dừng Bán</button>
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-danger" v-if="value.trang_thai == 1">Trang
                                            Chủ</button>
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-warning" v-else>Bán Hàng</button>
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        {{-- v-on:click="edit_Nha_Cung_Cap = Object.assign({}, value)" --}}
                                        <button v-on:click="edit_sach = Object.assign({}, value)"
                                            class=" btn btn-outline-info btn-sm m-1"
                                            data-bs-toggle="modal"data-bs-target="#updateModal">Cập nhật</button>
                                        {{-- v-on:click="del_Nha_Cung_Cap = value" --}}
                                        <button v-on:click="del_sach = value" class=" btn btn-outline-dark btn-sm"
                                            data-bs-toggle="modal"data-bs-target="#deleteModal">Huỷ bỏ</button>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                    {{-- Modal update --}}
                    <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Cập Nhật Sách</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mb-3">
                                        <label class="form-label">Mã Sách</label>
                                        <input v-model="edit_sach.ma_sach" type="text" class="form-control"
                                            placeholder="Nhập mã sách">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Tên Sách</label>
                                        <input v-model="edit_sach.ten_sach" type="text" class="form-control"
                                            placeholder="Nhập tên sách">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Thể Loại</label>
                                        <select v-model="edit_sach.id_the_loai" class="form-control">
                                            <option value="0">Vui lòng chọn thể loại sách</option>
                                            @foreach ($theLoai as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->ten_the_loai }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Giá Bán</label>
                                        <input v-model="edit_sach.gia_ban" type="text" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Mã Tác Giả</label>
                                        <select v-model="edit_sach.ma_tac_gia" class="form-control">
                                            <option value="0">Vui lòng chọn tác giả</option>
                                            @foreach ($tacGia as $value)
                                            <option value="{{$value->id}}">{{$value->ten_tac_gia}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-goup mb-3 ">
                                        <label class="form-label">Ảnh</label>
                                        <input class="form-control " type="file" ref="file"
                                            v-on:change="updateFile()" accept="image/">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Thông Tin</label>
                                        <textarea v-model="edit_sach.thong_tin" class="form-control" cols="30" rows="5"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Tình Trạng</label>
                                        <select v-model="edit_sach.tinh_trang" class="form-control">
                                            <option value="-1">Vui lòng chọn tình trạng sách</option>
                                            <option value="1">Đang Bán</option>
                                            <option value="0">Dừng Bán</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Trạng Thái</label>
                                        <select v-model="edit_sach.trang_thai" class="form-control">
                                            <option value="-1">Vui lòng chọn trạng thái sách</option>
                                            <option value="1">Trang Chủ</option>
                                            <option value="0">Bán hàng</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                    <button v-on:click="accpectUpdate()" type="button" class="btn btn-warning">Xác
                                        Nhận</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- modal delete --}}
                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Xóa Khu Vục</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-primary" role="alert">
                                        Bạn có chắc chắn muốn xóa sách: <b
                                            class="text-danger text-uppercase">@{{ del_sach.ten_sach }}</b> này không?
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger" v-on:click="accpectDelete()"
                                        data-bs-dismiss="modal">Xác Nhận</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list: [],
                    add_sach: {
                        'ma_sach': '',
                        'ten_sach': '',
                        'id_the_loai': 0,
                        'gia_ban': '',
                        'ma_tac_gia': 0,
                        'thong_tin': '',
                        'hinh_anh': '',
                        'tinh_trang': -1,
                        'trang_thai': -1,

                    },
                    file: '',
                    edit_sach: {
                        'ma_sach': '',
                        'ten_sach': '',
                        'id_the_loai': 0,
                        'gia_ban': '',
                        'ma_tac_gia': 0,
                        'thong_tin': '',
                        'hinh_anh': '',
                        'tinh_trang': -1,
                        'trang_thai': -1,
                    },
                    del_sach: '',
                    trang_thai : 0,
                },
                created() {
                    this.loadData();
                },
                methods: {
                    //Format giá bán
                    number_format(number) {
                        return new Intl.NumberFormat('vi-VI', {
                            style: 'currency',
                            currency: 'VND'
                        }).format(number);
                    },
                    //Hàm thêm mới + upFile
                    updateFile() {
                        this.file = this.$refs.files.files[0];

                    },
                    addSach() {
                        var formData = new FormData();
                        formData.append('hinh_anh', this.file);
                        formData.append('ma_sach', this.add_sach.ma_sach);
                        formData.append('ten_sach', this.add_sach.ten_sach);
                        formData.append('id_the_loai', this.add_sach.id_the_loai);
                        formData.append('gia_ban', this.add_sach.gia_ban);
                        formData.append('ma_tac_gia', this.add_sach.ma_tac_gia);
                        formData.append('thong_tin', this.add_sach.thong_tin);
                        formData.append('tinh_trang', this.add_sach.tinh_trang);
                        formData.append('trang_thai', this.add_sach.trang_thai);

                        axios
                            .post('/admin/quan-ly-sach/create', formData, {
                                headerf: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    this.add_sach = {
                                        'ma_sach': '',
                                        'ten_sach': '',
                                        'id_the_loai': 0,
                                        'gia_ban': '',
                                        'ma_tac_gia': 0,
                                        'thong_tin': '',
                                        'hinh_anh': '',
                                        'tinh_trang': -1,
                                        'trang_thai': -1,
                                    };
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                })
                                $("#add").removeAttr("disabled");
                            });
                    },
                    // Hàm đẩy dữ liệu lên serve
                    loadData() {
                        axios
                            .get('/admin/quan-ly-sach/data')
                            .then((res) => {
                                this.list = res.data.list;
                            });
                    },
                    //hàm đổi trạng thái và tình trạng
                    doiTrangThai(payLoad) {
                        axios
                            .post('/admin/quan-ly-sach/doi-trang-thai', payLoad)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    doiTinhTrang(payLoad) {
                        axios
                            .post('/admin/quan-ly-sach/doi-tinh-trang', payLoad)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    //Hàm cập nhật
                    accpectUpdate() {
                        axios
                            .post('/admin/quan-ly-sach/update', this.edit_sach)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    $('#updateModal').modal('hide');
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                })
                                $("#add").removeAttr("disabled");
                            });
                    },
                    accpectDelete() {
                        axios
                            .post('/admin/quan-ly-sach/delete', this.del_sach)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    $('#updateModal').modal('hide');
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    checkMaSach() {
                        var payload = {
                            'ma_sach': this.add_sach.ma_sach,
                            'ten_sach': this.add_sach.ten_sach,
                        };
                        axios
                            .post('/admin/quan-ly-sach/check-ma-sach', payload)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    $("#add").removeAttr("disabled");
                                } else if (res.data.status == 0) {
                                    toastr.error(res.data.message, "Error");
                                    $("#add").prop('disabled', true);
                                } else if (res.data.status == 2) {
                                    toastr.warning(res.data.message, "Warning");
                                }
                            });
                    },
                    checkTenSach() {
                        var payload = {
                            'ten_sach': this.add_sach.ten_sach,
                        };
                        axios
                            .post('/admin/quan-ly-sach/check-ten-sach', payload)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    $("#add").removeAttr("disabled");
                                } else if (res.data.status == 0) {
                                    toastr.error(res.data.message, "Error");
                                    $("#add").prop('disabled', true);
                                } else if (res.data.status == 2) {
                                    toastr.warning(res.data.message, "Warning");
                                }
                            });
                    },
                }
            });
        });
    </script>
@endsection
