@extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-md-3">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header text-center">
                    <b>Thêm Mới Thể Loại Sách</b>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Mã Thể Loại</label>
                        <input v-model="add_the_loai.ma_the_loai" type="text" class="form-control"
                            placeholder="Nhập mã thể loại">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tên Thể Loại</label>
                        <input v-model="add_the_loai.ten_the_loai" type="text" class="form-control"
                            placeholder="Nhập tên thể loại">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tình Trạng</label>
                        <select v-model="add_the_loai.tinh_trang" class="form-control">
                            <option value="1">Còn Hoạt Động</option>
                            <option value="0">Dừng Hoạt Động</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button v-on:click="addTheLoai()" class="btn btn-primary">Thêm Mới</button>
                </div>

            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header text-center">
                    <b> Danh Sách Các Thể Loại Sách</b>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="listNhaCungCap">
                        <thead>

                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Mã Thể Loại</th>
                                <th class="text-center">Tên Thể Loại</th>
                                <th class="text-center">Tình Trạng</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(value, key) in list">
                                <tr>
                                    <th class="text-center align-middle">
                                        <input type="checkbox" v-model="value.check">
                                    </th>
                                    <td class="align-middle text-nowrap text-center"> @{{ value.ma_the_loai }} </td>
                                    <td class="align-middle text-nowrap text-center"> @{{ value.ten_the_loai }} </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-danger" v-if="value.tinh_trang == 1">Còn Hoạt
                                            Động</button>
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-warning" v-else>Dừng Hoạt Động</button>
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        {{-- v-on:click="edit_Nha_Cung_Cap = Object.assign({}, value)" --}}
                                        <button class=" btn btn-outline-info btn-sm m-1"
                                            data-bs-toggle="modal"data-bs-target="#updateModal">Cập nhật</button>
                                        {{-- v-on:click="del_Nha_Cung_Cap = value" --}}
                                        <button v-on:click="del_the_loai = value" class=" btn btn-outline-dark btn-sm"
                                            data-bs-toggle="modal"data-bs-target="#deleteModal">Huỷ bỏ</button>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                    {{-- modal delete --}}
                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Xóa Khu Vục</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-primary" role="alert">
                                        Bạn có chắc chắn muốn xóa sách: <b
                                            class="text-danger text-uppercase">@{{ del_the_loai.ten_the_loai }}</b> này không?
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger" v-on:click="accpectDel()"
                                        data-bs-dismiss="modal">Xác Nhận</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list: [],
                    add_the_loai: {
                        'ma_the_loai': '',
                        'ten_the_loai': '',
                        'tinh_trang': 1
                    },
                    del_the_loai: {},
                },
                created() {
                    this.loadData();
                },
                methods: {
                    // Hàm đẩy dữ liệu lên serve
                    loadData() {
                        axios
                            .get('/admin/the-loai/data')
                            .then((res) => {
                                this.list = res.data.list;
                            });
                    },
                    //Hàm thêm mới
                    addTheLoai() {
                        axios
                            .post('/admin/the-loai/create', this.add_the_loai)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    this.add_the_loai = {
                                        'ma_the_loai': '',
                                        'ten_the_loai': '',
                                        'tinh_trang': 1
                                    };
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    //Hàm Đổi tình trạng
                    doiTrangThai(payLoad) {
                        axios
                            .post('/admin/the-loai/doi-trang-thai', payLoad)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                } else if (res.data.status == 0) {
                                    toastr.error(res.data.message, "Error");
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    //Hàm xoá bỏ
                    accpectDel() {
                        axios
                            .post('/admin/the-loai/delete', this.del_the_loai)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                } else if (res.data.status == 0) {
                                    toastr.error(res.data.message, "Error");
                                } else if (res.data.status == 2) {
                                    toastr.error(res.data.message, "Error");
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    }
                }
            });
        });
    </script>
@endsection
