<div class="nav-container primary-menu">
    <div class="mobile-topbar-header">
        <div>
            <img src="/assets_admin/images/logo-icon.png" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Rukada</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <nav class="navbar navbar-expand-xl w-100">
        <ul class="navbar-nav justify-content-start flex-grow-1 gap-1">
            {{-- <li class="nav-item dropdown">
                <a href="javascript:;" class="nav-link dropdown-toggle dropdown-toggle-nocaret"
                    data-bs-toggle="dropdown">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
                </a>
                <ul class="dropdown-menu">
                    <li> <a class="dropdown-item" href="index.html"><i class="bx bx-right-arrow-alt"></i>Default</a>
                    </li>
                    <li> <a class="dropdown-item" href="index2.html"><i class="bx bx-right-arrow-alt"></i>Alternate</a>
                    </li>
                    <li> <a class="dropdown-item" href="index3.html"><i class="bx bx-right-arrow-alt"></i>Graphical</a>
                    </li>
                </ul>
            </li> --}}
            <li class="nav-item dropdown">
                <a href="/trang-chu" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
                    <div class="parent-icon"><i class="fa-solid fa-house"></i>
                    </div>
                    <div class="menu-title">Trang chủ</div>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="/admin/ban-hang" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
                    <div class="parent-icon"><i class="fa-solid fa-book"></i>
                    </div>
                    <div class="menu-title">Sách</div>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a href="" class="nav-link dropdown-toggle dropdown-toggle-nocaret"
                    data-bs-toggle="dropdown">
                    <div class="parent-icon"><i class="fa-solid fa-book-medical"></i>
                    </div>
                    <div class="menu-title">Thể Loại</div>
                </a>
                <ul class="dropdown-menu">
                    @foreach ($theLoai as $value)
                    <li> <a class="dropdown-item" href="/admin/ban-hang/the-loai-sach/{{$value->id}}"><i class="bx bx-right-arrow-alt"></i>{{$value->ten_the_loai}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a href="" class="nav-link dropdown-toggle dropdown-toggle-nocaret"
                    data-bs-toggle="dropdown">
                    <div class="parent-icon"><i class="fa-solid fa-user-astronaut"></i>
                    </div>
                    <div class="menu-title">Tác Giả</div>
                </a>
                <ul class="dropdown-menu">
                    @foreach ($tacGia as $value)
                    <li> <a class="dropdown-item" href="/admin/ban-hang/tac-gia/{{$value->id}}"><i class="bx bx-right-arrow-alt"></i>{{$value->ten_tac_gia}}</a></li>
                    @endforeach
                </ul>
            </li>
            {{-- <li class="nav-item dropdown">
                <a href="" class="nav-link dropdown-toggle dropdown-toggle-nocaret"
                    data-bs-toggle="dropdown">
                    <div class="parent-icon"><i class="fa-solid fa-bars"></i>
                    </div>
                    <div class="menu-title">Danh Mục Sản Phẩm</div>
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="index.html"><i class="bx bx-right-arrow-alt">
                        </i>Thể Loại</a>
                    </li>
                    <li> <a class="dropdown-item" href="index2.html"><i class="bx bx-right-arrow-alt"></i>Tác Giả</a>
                    </li>
                    <li> <a class="dropdown-item" href="index3.html"><i class="bx bx-right-arrow-alt"></i>Xem Thêm</a>
                    </li>
                </ul>
            </li> --}}

    </nav>
</div>

